Categories:Navigation
License:GPLv3
Web Site:
Source Code:https://github.com/mapsquare/osm-contributor
Issue Tracker:https://github.com/mapsquare/osm-contributor/issues

Auto Name:Osm Contributor
Summary:OpenStreetMap Contributor Mapping Tool
Description:
The Openstreetmap Contributor app allows anyone to contribute to OSM. It enables
those-who-know to easily manage a group of newbies as the contribution process
is intuitive and easy. The App comes in three flavours: store (for the Android
Store version), poi-storage (for MapSquare POI Databases), and template (for the
osm.mapsquare.io tool for Mapping parties). Bring your MapParties to a whole new
level!
.

Repo Type:git
Repo:https://github.com/mapsquare/osm-contributor

Build:2.2.2,14
    disable=requires bing maps api key (none in the repo)
    commit=2.2.2
    gradle=yes
    gradleprops=foss=true

Auto Update Mode:None
Update Check Mode:Tags
Current Version:3.0.0-beta.3
Current Version Code:17
